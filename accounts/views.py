from django.contrib.auth.models import User
from django.contrib.auth import login
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm

# Create your views here.


def sign_up_form(request):

    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            user.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm()

        context = {
            "form": form,
        }
        return render(request, "registration/signup.html", context)


# function signup(request)
#     if the request method is "POST", then
#         form = UserCreationForm(request.POST)
#         if the form is valid, then
#             username = the "username" value from the request.POST dictionary
#             password = the "password1" value from the request.POST dictionary
#             user = create a new user using username and password
#             save the user object
#             login the user with the request and user object
#             return a redirect to the "home" path
#     otherwise,
#         form = UserCreationForm(request.POST)
#     context = dictionary with key "form" and value form
#     return render with request, "registration/signup.html", and the context
